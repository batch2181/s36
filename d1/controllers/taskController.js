const Task = require("../models/task.js");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result; 
	})
};

module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})
	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error)
			return false;
		} else {
			return savedTask;
		}
		
	})
};



module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return "Task " + removedTask.name + " has been removed!" ;
		}
	})
};

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		result.name = newContent.name
		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false
			} else {
				return updateTask;
			}
		})
	})
};

// S36 ACTIVITY

module.exports.getOneTask = (taskId) => {
	return Task.findById(taskId).then(result =>{
			return result;
	})
};

module.exports.updateStatus  = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 
		result.status = newStatus.status
		return result.save().then((updatedStatus, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedStatus;
			}
		})
	})
};


