// A L L   E N D P O I N T S

const express = require("express");

const TaskController = require("../controllers/taskController.js");

const router = express.Router();

router.get("/viewTasks", (req, res) => {
    TaskController.getAllTasks().then(resultFromController => 
    res.send(resultFromController))
});

router.post('/createTask', (req, res) => {
	TaskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
});

router.delete("/deleteTask/:id", (req, res) => {
    TaskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
});

router.put("/updateTask/:id", (req, res) => {
    TaskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
});

// S36 ACTIVITY

router.get("/getOneTask/:id", (req, res) => {
    TaskController.getOneTask(req.params.id).then(resultFromController => res.send(resultFromController))
});

router.put("/updateStatus/:id", (req, res) => {
    TaskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
});


module.exports = router;    